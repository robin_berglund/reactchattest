import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ChatWindow from './ChatWindow.js'

class App extends Component {

constructor(props) {
  super(props);
  this.state = {
    messages: [
      {"message": "wohoo", "sender": "Robin", "time": Date.now()}]
  }
  this.addMessage = this.addMessage.bind(this);
}

addMessage(message){
  let messages = this.state.messages;
  messages.push(message);
  this.setState({
    messages: messages
  })
}

  render() {
    return (
      <div className="App">
        <ChatWindow messages={this.state.messages} addMessage={this.addMessage}/>
      </div>
    );
  }
}

export default App;
