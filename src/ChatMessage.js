import React, { Component } from 'react';


class ChatMessage extends Component {

    constructor(props) {
        super(props);
        this.formatDate = this.formatDate.bind(this);        
    }

    formatDate(unix_timestamp)
    {
        let formattedTime = new Date(unix_timestamp)
        return formattedTime.toString();
    }

    render() {
        return (
            <div>
                <span><b>{this.formatDate(this.props.time)}: </b></span><br/>
                <span><b>{this.props.sender}: </b></span>
                <span>{this.props.message}</span>
                
            </div>            
        )
    }
}

export default ChatMessage