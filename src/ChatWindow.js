import React, { Component } from 'react';
import ChatMessage from './ChatMessage'

class ChatWindow extends Component {
   constructor(props) {
        super(props);
        this.state = {
            message: "",
            name : "Robin",
            date: ""
        };
    }
sendMessage(e){
    e.preventDefault();
    const message = {"message": this.state.message, "sender": this.state.name, "time": Date.now()}
    this.props.addMessage(message)
    this.setState({
        message: ""
    })
}

handleChange(e) {
    this.setState({
        message: e.target.value
    })
}
  render() {

    return(
        <div>   
            <div>
                {
                    this.props.messages.map((item) => 
                    <ChatMessage sender={item.sender} time={item.time} message={item.message}/>
                )}
            </div>
            <form onSubmit={this.sendMessage.bind(this)}>
                <input type="text" placeholder="Skriv meddelande" onChange={e => this.handleChange(e)} value={this.state.message}/>
                <input type="submit" value="Skicka"/>
            </form>  
        </div>
    );
  }
}

export default ChatWindow;